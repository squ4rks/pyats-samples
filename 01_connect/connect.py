from genie.testbed import load

tb = load('testbed.yaml')

for name, dev in tb.devices.items():
    dev.connect(log_stdout=False)