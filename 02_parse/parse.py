from genie.testbed import load

tb = load('testbed.yaml')

for name, dev in tb.devices.items():
    dev.connect(log_stdout=False)

    out = dev.parse('show version')
    print(f"{name}: {out['version']['version_short']}")