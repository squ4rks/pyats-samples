from pyats import aetest
import logging
import os

logger = logging.getLogger(__name__)

class CommonSetup(aetest.CommonSetup):
    @aetest.subsection
    def connect_to_devices(self, testbed):
        for device in testbed:
            # don't do the default show version
            # don't do the default config
            if device.os != "iosxe":
                continue
            device.connect(init_exec_commands=[],
                           init_config_commands=[],
                           log_stdout=False)
            
            logger.info('{device} connected'.format(device=device.alias))

class CheckVersion(aetest.Testcase):
    @aetest.test
    def check_current_version(self, testbed):
        test_success = True
        xe_version = '16.9.3'

        for device in testbed:
            if device.os != "iosxe":
                continue
            #Learning platform information
            platform = device.learn('platform') 
            if (platform.os.lower() == 'iosxe' and platform.version != xe_version): 
                test_success = False
                logger.error(f"{device.alias} is {platform.version} - should be {xe_version}")

            logger.debug('{device} running {platformos} has OS: {os}'.format(device=device.alias, platformos=platform.os, os=platform.version))
        
        assert test_success == True

class CommonCleanup(aetest.CommonCleanup):

    @aetest.subsection
    def disconnect_from_devices(self, testbed):
        for device in testbed:
            device.disconnect()

if __name__ == '__main__':
    # local imports
    from genie.testbed import load
    import sys

    # set debug level DEBUG, INFO, WARNING
    logger.setLevel(logging.INFO)

    # Loading device information
    testbed = load('testbed.yaml')

    aetest.main(testbed = testbed)
